package compiler;

import compiler._nfa.NfaInterpreter;
import compiler.input.Input;
import compiler.lexer.Lexer;
import compiler.lexer.MacroHandler;
import compiler.lexer.RegularExpressionHandler;
import compiler._nfa.NfaMachineConstructor;
import compiler._nfa.NfaPair;
import compiler._nfa.NfaPrinter;

/**
 * Created by szj on 2017/2/2.
 */
public class NfaTest {

    public static void main(String[] arg0){
        Input input = new Input();
        MacroHandler macroHandler = new MacroHandler(input);
        RegularExpressionHandler regularExpr = new RegularExpressionHandler(input, macroHandler);
        Lexer lexer = new Lexer(regularExpr);
        NfaMachineConstructor constructor = new NfaMachineConstructor(lexer);
        NfaPair nfaPair = new NfaPair();
//        constructor.constructNfaForSingleCharacter(nfaPair);
//        constructor.constructNfaForDot(nfaPair);
//        constructor.constructNfaForCharacterSetWithoutNegative(nfaPair);
//        constructor.constructNfaForCharacterSet(nfaPair);
//        constructor.factor(nfaPair);
//        constructor.catExpr(nfaPair);
        constructor.expr(nfaPair);
        NfaPrinter printer = new NfaPrinter();
        printer.printNfa(nfaPair.startNode);
        NfaInterpreter nfaInterpreter = new NfaInterpreter(nfaPair.startNode, input);
        nfaInterpreter.interpretNfa();
    }
}
