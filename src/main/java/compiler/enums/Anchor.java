package compiler.enums;

public enum Anchor {
    NONE,
    START,
    END,
    BOTH
}

