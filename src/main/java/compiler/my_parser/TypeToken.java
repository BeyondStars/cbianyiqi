package compiler.my_parser;

/**
 * Created by szj on 2017/5/30.
 */
public enum TypeToken {
    //non-termals
    PROGRAM, EXT_DEF_LIST, EXT_DEF, OPT_SPECIFIERS, EXT_DECL_LIST,
    EXT_DECL, VAR_DECL, SPECIFIERS,

    //stmt, expr, term, factor,
    TYPE_OR_CLASS, TYPE_NT,
    TYPE_SPECIFIER, NEW_NAME, NAME_NT,

    //terminals
    NAME, TYPE, CLASS, LP, RP, LB, RB, PLUS,

    //NUM, TIMES,
    COMMA, SEMI, WHITE_SPACE, EQUAL, TTYPE, STAR, UNKNOWN_TOKEN;

}
