package compiler._nfa;

import compiler.constant.LexerConstant;
import compiler.enums.Anchor;

import java.util.HashSet;

public class Nfa {
    public static final int EPSILON = -1; //边对应的是ε
    public static final int CCL = -2; //边对应的是字符集
    public static final int EMPTY = -3; //该节点没有出去的边

    public HashSet<Byte> inputSet = new HashSet<>(); //用来存储字符集类
    public Nfa next;  //跳转的下一个状态，可以是空
    public Nfa next2; //跳转的另一个状态，当状态含有两条ε边时，这个指针才有效
    private Anchor anchor;  //对应的正则表达式是否开头含有^, 或结尾含有$,  或两种情况都有
    private int stateNum; //节点编号
    private boolean visited = false; //节点是否被访问过，用于节点打印
    private int edge; //记录转换边对应的输入，输入可以是空, ε，字符集(CCL),或空，也就是没有出去的边

    public void addToSet(byte character) {
        this.inputSet.add(character);
    }

    public void setComplement() {
        HashSet<Byte> newSet = new HashSet<>();
        for (byte c = 0; c < LexerConstant.ASCII_COUNT - 1; c++) {
            if (!inputSet.contains(c)) {
                newSet.add(c);
            }
        }
        inputSet = newSet;
    }

    public int getStateNum() {
        return stateNum;
    }

    public void setStateNum(int num) {
        stateNum = num;
    }

    public void clearState() {
        inputSet.clear();
        next = next2 = null;
        anchor = Anchor.NONE;
        stateNum = -1;
    }

    public void setVisited() {
        visited = true;
    }

    public boolean isVisited() {
        return visited;
    }

    public int getEdge() {
        return edge;
    }

    public void setEdge(int type) {
        edge = type;
    }
}
