package compiler.compiler;

public class Specifier {
    //type
    public static int NONE = -1;
    public static int INT = 0;
    public static int CHAR = 1;
    public static int VOID = 2;
    public static int STRUCTURE = 3;
    public static int LABEL = 4;

    //storage
    public static int FIXED = 0;
    public static int REGISTER = 1;
    public static int AUTO = 2;
    public static int TYPEDEF = 3;
    public static int CONSTANT = 4;

    public static int NO_OCLASS = 0;  //如果内存类型是auto, 那么存储类型就是NO_OCLASS
    public static int PUBLIC = 1;
    public static int PRIVATE = 2;
    public static int EXTERN = 3;
    public static int COMMON = 4;

    private int basicType;
    private int storageClass;
    private int outputClass = NO_OCLASS;
    private boolean isLong = false;
    private boolean isSigned = false;
    private boolean isStatic = false;
    private boolean isExternal = false;
    private int constantValue = 0;
    private StructDefine vStruct;

    public int getType() {
        return basicType;
    }

    public void setType(int type) {
        basicType = type;
    }

    public int getStorageClass() {
        return storageClass;
    }

    public void setStorageClass(int s) {
        storageClass = s;
    }

    public int getOutputClass() {
        return outputClass;
    }

    public void setOutputClass(int c) {
        outputClass = c;
    }

    public boolean getLong() {
        return isLong;
    }

    public void setLong(boolean l) {
        isLong = l;
    }

    public boolean isSign() {
        return isSigned;
    }

    public void setSign(boolean signed) {
        isSigned = signed;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean s) {
        isStatic = s;
    }

    public boolean isExternal() {
        return isExternal;
    }

    public void setExternal(boolean e) {
        isExternal = e;
    }

    public int getConstantVal() {
        return constantValue;
    }

    public void setConstantVal(int v) {
        constantValue = v;
    }
}
